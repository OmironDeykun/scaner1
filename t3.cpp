#include <iostream>

#include <event2/event.h>
#include <event2/bufferevent.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
// for  convert a command-line argument to int
#include <cstdlib>



void bufev_callback( struct bufferevent *buf_ev, short events, void *ptr )
{
  if( events & BEV_EVENT_CONNECTED )
  {
    std::cout << "Connected \n";
    *((int *)ptr) = 1;
  }
  else if( events & BEV_EVENT_ERROR )
  {
    std::cout << "error \n";
    *((int *)ptr) = -1;
  }
  else if (events & BEV_EVENT_TIMEOUT)
  {
    std::cout << "timeout awd \n";
    *((int *)ptr) = 0;
  }
}


class worker
{
private:
	struct event_base* eb;
public:
	int start(){
		if((eb = event_base_new()) == NULL) {
	        return -1;
	    }
	}
	int stop(){}
	event_base* getBase(){
		return eb;
	}
};




class CPortScanner;
// typedef void (*PortScanner_cb)(CPortScanner *);
typedef void (*PortScanner_cb)(struct bufferevent *buf_ev, short events, void *ptr);

// typedef void (*PortScanner_cb)(struct , short, void *);

class CPortScanner
{
private:
	char* ip;
	int port;
	// PortScanner_cb cb;
	// int status;
	int status;
	event_base* base;

public:
	// CPortScanner(int ip, int port, PortScanner_cb);
	// CPortScanner(int ip, int port);
	// PortScanner_cb cbf;
	CPortScanner(char* ipIn, int portIn, PortScanner_cb cbIn, event_base* baseIn)
	{
		ip = ipIn;
		port = portIn;
		// cb = cbIn;
		base = baseIn;
		PortScanner_cb cb;

	}
	

	void check()
	{
		void(*bcb)(struct bufferevent *buf_ev, short events, void *ptr);

		// struct event_base *base;
		struct bufferevent *buf_ev;
		struct sockaddr_in sin;

		struct timeval tv = {1, 0};

		// base = event_base_new();

		memset( &sin, 0, sizeof(sin) );
		sin.sin_family      = AF_INET;            
		sin.sin_addr.s_addr = inet_addr(ip);
		sin.sin_port        = htons(port);

		buf_ev = bufferevent_socket_new( base, -1, BEV_OPT_CLOSE_ON_FREE );
		// bufferevent_setcb( buf_ev, NULL, NULL, bcb, &status );
		bufferevent_setcb( buf_ev, NULL, NULL, bufev_callback, &status );

		bufferevent_set_timeouts(buf_ev, &tv, &tv);
		if( bufferevent_socket_connect( buf_ev, (struct sockaddr *)&sin, sizeof(sin) ) < 0 )
		{
		  bufferevent_free( buf_ev ); 
		  // return -1;
		}

		event_base_dispatch( base );

		std::cout << "ip: " << ip << ", port: " << port << ", status: " << status << "\n";
	}
};

int main(int argc, char const *argv[])
{
	worker w;
	w.start();
	event_base* b = w.getBase();


	char ip[50] = "18.194.217.232";
	int  port   = 22;
	CPortScanner scanner( ip, port, bufev_callback, b );
	scanner.check();

	return 0;
}
